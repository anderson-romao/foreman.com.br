$(document).ready(function(){
	// HOME -> modadelidades
	$('.carousel-modadelidades').slick({
		infinite: true,
		slidesToShow: 1,
		slidesToScroll: 1,
		autoplay: true,
		autoplaySpeed: 7000,
		arrows: true,
		dots: false,
		prevArrow:'<img src="imagens/logos/arrow-left.png" class="btn-slick-carousel-left" alt="Anterior" title="Anterior">',
		nextArrow:'<img src="imagens/logos/arrow-right.png" class="btn-slick-carousel-right" alt="Próximo" title="Próximo">',
		pauseOnHover: true,
		// responsive: [
		// {
		// 	breakpoint: 1921,
		// 	settings: {
		// 		slidesToShow: 4,
		// 	}
		// }, {
		// 	breakpoint: 1661,
		// 	settings: {
		// 		slidesToShow: 3,
		// 	}
		// }, {
		// 	breakpoint: 1025,
		// 	settings: {
		// 		slidesToShow: 2,
		// 	}
		// }, {
		// 	breakpoint: 800,
		// 	settings: {
		// 		slidesToShow: 2,
		// 	}
		// }, {
		// 	breakpoint: 520,
		// 	settings: {
		// 		slidesToShow: 1,
		// 	}
		// }
		// ]
	});

	// HOME -> estrutura
	$('.carousel-estrutura').slick({
		infinite: true,
		slidesToShow: 1,
		slidesToScroll: 1,
		autoplay: true,
		autoplaySpeed: 6000,
		arrows: true,
		dots: false,
		prevArrow:'<img src="imagens/logos/arrow-left.png" class="btn-slick-carousel-left" alt="Anterior" title="Anterior">',
		nextArrow:'<img src="imagens/logos/arrow-right.png" class="btn-slick-carousel-right" alt="Próximo" title="Próximo">',
		pauseOnHover: true,
	});

	// HOME -> depoimentos
	$('.carousel-depoimentos').slick({
		infinite: true,
		slidesToShow: 1,
		slidesToScroll: 1,
		autoplay: true,
		autoplaySpeed: 8000,
		arrows: true,
		dots: false,
		prevArrow:'<img src="imagens/logos/arrow-left.png" class="btn-slick-carousel-left" alt="Anterior" title="Anterior">',
		nextArrow:'<img src="imagens/logos/arrow-right.png" class="btn-slick-carousel-right" alt="Próximo" title="Próximo">',
		pauseOnHover: true,
	});

	// SOBRE -> historia
	$('.carousel-historia').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: false,
		autoplay: true,
		autoplaySpeed: 5000,
		fade: true,
		asNavFor: '.slider-nav'
	});

	// SOBRE -> historia
	$('.slider-nav').slick({
		slidesToShow: 4,
	  	slidesToScroll: 1,
	  	asNavFor: '.carousel-historia',
	  	dots: false,
	  	arrows: true,
		prevArrow:'<img src="imagens/logos/arrow-left-3.png" class="btn-slick-carousel-left" alt="Anterior" title="Anterior">',
		nextArrow:'<img src="imagens/logos/arrow-right-3.png" class="btn-slick-carousel-right" alt="Próximo" title="Próximo">',
	  	centerMode: false,
	  	focusOnSelect: true,
	  	responsive: [
	  	{
	  		breakpoint: 1200,
	  		settings: {
	  			slidesToShow: 4,
	  		}
	  	}, {
			breakpoint: 1025,
			settings: {
				slidesToShow: 3,
			}
		}, {
			breakpoint: 800,
			settings: {
				slidesToShow: 2,
			}
		}, {
			breakpoint: 520,
			settings: {
				slidesToShow: 1,
			}
		}
	  	]
	});

	// CONVENIOS -> convenios
	$('.carousel-convenios').slick({
		infinite: true,
		slidesToShow: 4,
		slidesToScroll: 1,
		autoplay: true,
		autoplaySpeed: 5000,
		arrows: true,
		dots: false,
		prevArrow:'<img src="imagens/logos/arrow-left.png" class="btn-slick-carousel-left" alt="Anterior" title="Anterior">',
		nextArrow:'<img src="imagens/logos/arrow-right.png" class="btn-slick-carousel-right" alt="Próximo" title="Próximo">',
		pauseOnHover: true,
		responsive: [
		{
			breakpoint: 1025,
			settings: {
				slidesToShow: 3,
			}
		}, {
			breakpoint: 800,
			settings: {
				slidesToShow: 2,
			}
		}, {
			breakpoint: 520,
			settings: {
				slidesToShow: 1,
			}
		}
		]
	});

	// CONVENIOS -> parceiros
	$('.carousel-parceiros').slick({
		infinite: true,
		slidesToShow: 4,
		slidesToScroll: 1,
		autoplay: true,
		autoplaySpeed: 5000,
		arrows: true,
		dots: false,
		prevArrow:'<img src="imagens/logos/arrow-left.png" class="btn-slick-carousel-left" alt="Anterior" title="Anterior">',
		nextArrow:'<img src="imagens/logos/arrow-right.png" class="btn-slick-carousel-right" alt="Próximo" title="Próximo">',
		pauseOnHover: true,
		responsive: [
		{
			breakpoint: 1025,
			settings: {
				slidesToShow: 3,
			}
		}, {
			breakpoint: 800,
			settings: {
				slidesToShow: 2,
			}
		}, {
			breakpoint: 520,
			settings: {
				slidesToShow: 1,
			}
		}
		]
	});

	// MODALIDADE INTERNA -> outras modalidades
	$('.carousel-modalidades').slick({
		infinite: true,
		slidesToShow: 5,
		slidesToScroll: 1,
		autoplay: true,
		autoplaySpeed: 5000,
		arrows: true,
		dots: false,
		prevArrow:'<img src="imagens/logos/arrow-left.png" class="btn-slick-carousel-left" alt="Anterior" title="Anterior">',
		nextArrow:'<img src="imagens/logos/arrow-right.png" class="btn-slick-carousel-right" alt="Próximo" title="Próximo">',
		pauseOnHover: true,
		responsive: [
		{
			breakpoint: 1440,
			settings: {
				slidesToShow: 4,
			}
		}, {	
			breakpoint: 1025,
			settings: {
				slidesToShow: 3,
			}
		}, {
			breakpoint: 800,
			settings: {
				slidesToShow: 2,
			}
		}, {
			breakpoint: 520,
			settings: {
				slidesToShow: 1,
			}
		}
		]
	});

	// CONVENIOS -> parceiros
	$('.carousel-footer').slick({
		infinite: true,
		slidesToShow: 4,
		slidesToScroll: 1,
		autoplay: true,
		autoplaySpeed: 5000,
		arrows: false,
		dots: false,
		pauseOnHover: true,
		responsive: [
		{
			breakpoint: 1025,
			settings: {
				slidesToShow: 3,
			}
		}, {
			breakpoint: 800,
			settings: {
				slidesToShow: 2,
			}
		}, {
			breakpoint: 520,
			settings: {
				slidesToShow: 1,
			}
		}
		]
	});

	// CONVENIOS -> parceiros
	$('.carousel-artigos').slick({
		infinite: true,
		slidesToShow: 1,
		slidesToScroll: 1,
		autoplay: false,
		// autoplaySpeed: 5000,
		arrows: true,
		prevArrow:'<img src="imagens/logos/arrow-left-3.png" class="btn-slick-carousel-left" alt="Anterior" title="Anterior">',
		nextArrow:'<img src="imagens/logos/arrow-right-3.png" class="btn-slick-carousel-right" alt="Próximo" title="Próximo">',
		dots: false,
		pauseOnHover: true,
	});
});

//Iniciar animações
AOS.init({
	disable: 'mobile',
	delay: 0,
	duration: 1200,
	once: true,
});