/*-----------------------------------------
LGPD
-----------------------------------------*/
// Adiciona a classe ao body
document.body.className = document.body.className + " js_enabled";

// Variaveis do modal.
var modalLGPD = document.querySelector('.lgpd-cookies');
var botaoContinuar = document.querySelector('.lgpd-botao.continuar');
var botaoSair = document.querySelector('.lgpd-botao.sair');


/* Verifica se o modal já foi visualizado 
** e não o mostra novamente */
function primeiroAcesso() {
    /**
     * Set cookie
     *
     * @param string name
     * @param string value
     * @param int days
     * @param string path
     * @see http://www.quirksmode.org/js/cookies.html
    */

    function createCookie(name, value, days, path) {
        if (days) {
            var date = new Date();
            date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
            var expires = "; expires=" + date.toGMTString();
        } else var expires = "";
        document.cookie = name + "=" + value + expires + "; path=" + path;
    }

    /**
     * Read cookie
     * @param string name
     * @returns {*}
     * @see http://www.quirksmode.org/js/cookies.html
    */

    function readCookie(name) {
        var nameEQ = name + "=";
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') c = c.substring(1, c.length);
            if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
        }
        return null;
    }

    // Clique no botão de continuar, fecha o modal e salva que já foi visualizado.
    botaoContinuar.addEventListener('click', function () {
        modalLGPD.style.display = 'none';
        createCookie('lgpd-PROJETO-visualizada', 'yes', cookieExpiry, cookiePath);
    });

    // Clique no botão de sair, limpa os cookies e retorna ao site acessado anteriormente.
    botaoSair.addEventListener('click', function () {
        modalLGPD.style.display = 'none';

        // Capta os cookies criados.
        var cookies = document.cookie.split("; ");

        // Deleta os cookies criados.
        for (var c = 0; c < cookies.length; c++) {
            var d = window.location.hostname.split(".");
            while (d.length > 0) {
                var cookieBase = encodeURIComponent(cookies[c].split(";")[0].split("=")[0]) +
                    '=; expires=Thu, 01-Jan-1970 00:00:01 GMT; domain=' + d.join('.') + ' ;path=';
                var p = location.pathname.split('/');
                document.cookie = cookieBase + '/';
                while (p.length > 0) {
                    document.cookie = cookieBase + p.join('/');
                    p.pop();
                };
                d.shift();
            }
        }

        createCookie('lgpd-PROJETO-visualizada', 'no', cookieExpiry, cookiePath);

        // Retona a página de navegação anterior.
        history.go(-1);
    });

    var cookieMessage = document.querySelector('.lgpd-cookies');

    if (cookieMessage == null) {
        return;
    }
    
    var cookie = readCookie('lgpd-PROJETO-visualizada');

    if (cookie != null && cookie == 'yes') {
        cookieMessage.style.display = 'none';
    } else {
        cookieMessage.style.display = 'flex';
    }

    // Configura / atualiza o cookie.
    var cookieExpiry = cookieMessage.getAttribute('data-cookie-expiry');
    if (cookieExpiry == null) {
        cookieExpiry = 60;
    }
    var cookiePath = cookieMessage.getAttribute('data-cookie-path');
    if (cookiePath == null) {
        cookiePath = "/";
    }
}

// Carrega o LGPD após a página carregar completamente.
window.onload = primeiroAcesso();